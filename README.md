# adult-swimages simple file upload API / admin console

## Pre-requisites

You should have `npm` installed.  `Grunt` is optional for running JSHint.

## Initial setup

    npm install

## Running the application locally

    npm start

## Running the tests

    npm test

## Code coverage

### Running code coverage

    npm run coverage

### Viewing results

    open coverage/lcov-report/index.html

## JSHint

    grunt

## REST API

Get an upload token (sets token in cookie)

    GET http://localhost:3000/token

Upload an image (token required in cookie)

    POST http://localhost:3000/upload

Parameters:

* title - the title of the media
* username - the username of the uploader
* email - the email address of the uploader
* credit_url - an optional URL to give credit to the uploader's site
* tags - an optional comma-delimited list of tags e.g. "action,funny,cartoon"


Get all image metadata

    GET http://localhost:3000/media?filter=all

Get today's image metadata

    GET http://localhost:3000/media?filter=today

Get image metadata from the past 7 days

    GET http://localhost:3000/media?filter=thisWeek

Get image metadata for a particular user

    GET http://localhost:3000/media?filter=username&username=[user name]

## Token DB maintenance
To ensure deletion of tokens from the DB after a set period of time (e.g. 5 minutes), execute the following:

    db.tokens.ensureIndex({created: 1}, {expireAfterSeconds: 300});

## Media DB maintenance

    db.media.ensureIndex({uploader: 1});
    db.media.ensureIndex({tags: 1});


