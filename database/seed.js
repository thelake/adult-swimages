var config = require('.././config');

config.setDev();

var db = require('../mongo');


var data = [];

data.push({"title" : "My badass Video", 
	"url" : "https://s3.amazonaws.com/turner-swimages/coaster04.jpg", 
	"uploader" : "donwb",
	"email" : "don.browning@turner.com", 
	"starred" : false,  
	"reviewed" : false,
  "published" : false,
  "size" : 143.98
});
data.push({"title" : "Image 1", 
    "url" : "https://s3.amazonaws.com/turner-swimages/coaster01.jpg", 
    "uploader" : "donwb",
    "email" : "don.browning@turner.com", 
    "starred" : false,  
    "reviewed" : false,
    "published" : false,
    "size" : 54.78
});
data.push({"title" : "In the park", 
    "url" : "https://s3.amazonaws.com/turner-swimages/coaster02.jpg", 
    "uploader" : "donwb",
    "email" : "don.browning@turner.com", 
    "starred" : false,  
    "reviewed" : false,
    "published" : false,
    "size" : 104.89
});
data.push({"title" : "Image 1", 
    "url" : "https://s3.amazonaws.com/turner-swimages/coaster03.jpg", 
    "uploader" : "donwb",
    "email" : "don.browning@turner.com", 
    "starred" : false,  
    "reviewed" : false,
    "published" : false,
    "size" : 81.14
});
data.push({"title" : "My badass Video", 
    "url" : "https://s3.amazonaws.com/turner-swimages/coaster05.jpg", 
    "uploader" : "donwb",
    "email" : "don.browning@turner.com", 
    "starred" : false,  
    "reviewed" : false,
    "published" : false,
    "size" : 83.19
});
data.push({"title" : "Image 1", 
    "url" : "https://s3.amazonaws.com/turner-swimages/coaster06.jpg", 
    "uploader" : "donwb",
    "email" : "don.browning@turner.com", 
    "starred" : false,  
    "reviewed" : false,
    "published" : false,
    "size" : 101.57
});
data.push({"title" : "In the park", 
    "url" : "https://s3.amazonaws.com/turner-swimages/coaster07.jpg", 
    "uploader" : "donwb",
    "email" : "don.browning@turner.com", 
    "starred" : false,  
    "reviewed" : false,
    "published" : false,
    "size" : 140.3
});
data.push({"title" : "Image 1", 
    "url" : "https://s3.amazonaws.com/turner-swimages/coaster08.jpg", 
    "uploader" : "donwb",
    "email" : "don.browning@turner.com", 
    "starred" : false,  
    "reviewed" : false,
    "published" : false,
    "size" : 124.93
});
data.push({"title" : "My badass Video", 
    "url" : "https://s3.amazonaws.com/turner-swimages/coaster09.jpg", 
    "uploader" : "donwb",
    "email" : "don.browning@turner.com", 
    "starred" : false,  
    "reviewed" : false,
    "published" : false,
    "size" : 48.71
});
data.push({"title" : "Image 1", 
    "url" : "https://s3.amazonaws.com/turner-swimages/coaster10.jpg", 
    "uploader" : "donwb",
    "email" : "don.browning@turner.com", 
    "starred" : false,  
    "reviewed" : false,
    "published" : false,
    "size" : 92.94
});
data.push({"title" : "In the park", 
    "url" : "https://s3.amazonaws.com/turner-swimages/coaster11.jpg", 
    "uploader" : "donwb",
    "email" : "don.browning@turner.com", 
    "starred" : false,  
    "reviewed" : false,
    "published" : false,
    "size" : 84.22
});
data.push({"title" : "Image 1", 
    "url" : "https://s3.amazonaws.com/turner-swimages/coaster12.jpg", 
    "uploader" : "donwb",
    "email" : "don.browning@turner.com", 
    "starred" : false,  
    "reviewed" : false,
    "published" : false,
    "size" : 88.62
});
data.push({"title" : "My badass Video", 
    "url" : "https://s3.amazonaws.com/turner-swimages/coaster13.jpg", 
    "uploader" : "neilshannon",
    "email" : "neil.shannon@turner.com",
    "starred" : false,  
    "reviewed" : false,
    "published" : false,
    "size" : 91.6
});
data.push({"title" : "Image 1", 
    "url" : "https://s3.amazonaws.com/turner-swimages/coaster14.jpg", 
    "uploader" : "neilshannon",
    "email" : "neil.shannon@turner.com",
    "starred" : false,  
    "reviewed" : false,
    "published" : false,
    "size" : 92.34
});
data.push({"title" : "In the park", 
    "url" : "https://s3.amazonaws.com/turner-swimages/coaster15.jpg", 
    "uploader" : "donwb",
    "email" : "don.browning@turner.com", 
    "starred" : false,  
    "reviewed" : false,
    "published" : false,
    "size" : 74.41
});
data.push({"title" : "Image 1", 
    "url" : "https://s3.amazonaws.com/turner-swimages/coaster16.jpg", 
    "uploader" : "donwb",
    "email" : "don.browning@turner.com", 
    "starred" : false,  
    "reviewed" : false,
    "published" : false,
    "size" : 108.88
});

/* Sample publish document */
var publishData = [];
publishData.push({"set" : [
    {
        "imageId" : "529250affc35ff0000000001",
        "url" : "https://s3.amazonaws.com/turner-swimages/coaster04.jpg"
    },
    {
        "imageId" : "529250affc35ff0000000001",
        "url" : "https://s3.amazonaws.com/turner-swimages/coaster04.jpg"
    }
]});



db.open(function(conn){
  db.removeAll('media', function doneRemovingMedia(){
    db.insert(data, 'media', function(json){
        db.removeAll('publish', function() {
            db.insert(publishData, 'publish', function(result){
                console.log(result);
            })
        });
      console.log(json);
    }); 
  });

  db.removeAll('tokens', function tokensRemoved(){
      db.insert({"token": 'testToken'}, 'tokens', function(json){
          console.log('inserted token [' + json + ']');
      });
  });
});





