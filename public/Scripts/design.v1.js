//handlebars template
var selectedImages = [];

var imageTemplate = [
        '{{#.}}',
        '<div class="item-wrapper">' +
			"<div class='item' data-url='{{url}}' id='{{_id}}' data-date='{{lastModified}}'>" +
				"<img class='lazy' data-original='{{{url}}}' width='200' height='200' />" +
			"</div>" +
			"<div class='itemoverlay'>" +
				"<div class='addtoqueue'><img src='./Content/images/plus.png' width='25' height='25' alt='' /><span>ADD TO QUEUE</span></div>" +
				"<div class='kill'><img src='./Content/images/no.png' width='25' height='25' alt='' /><span>KILL</span></div>" +
			"</div>" +
            '<div class="imgdata">' +
        		'<span class="metadata">' +
        			'<span class="uploader">Name: <span>{{uploader}}</span></span><br>' +
        			'<span class="credit">Image: <a target="_blank" href="{{url}}">{{credit_url}} ({{sizeFormat size}} kb)</a></span><br>' +
        			'<span class="tagslist">Tags: {{#each tags}}<span onclick="filterByTag(this)">{{this}}</span>,{{/each}}</span>' +
        		'</span>' +
        	'</div>' +
		'</div>',
        '{{/.}}'
].join("");

//dates array holds each individual date for filter
var dates = [];
//totalDates is the number of dates displayed. used to calc height of filter box and to not search for an empty index in array
var totalDates = 0;
//holds the selected dates for the filter
//var shownDates = [];

var f_dates = [], f_usernames = [], f_tags = [];

//document ready
$(document).ready(function(){
	//api call to get and render images into the gallery
	getImages();

	//sets click handler on body for items that aren't yet in the DOM
	//$('#kill').on('click', kill);
	$('.filter-link').on('click', filter);

	//open and close for the filters panel
	$(".filters .open").click(function() {
		$("body").toggleClass("filter");
	});

	$('#clearfilter').hide();
	$('#clearfilter').click(function () {
		f_dates = []; f_usernames = []; f_tags = [];
		$('.heading').text(getHeading());
		getImages();
	});

	Handlebars.registerHelper('sizeFormat', function(value){
	    return parseFloat(Math.round(value * 100) / 100).toFixed(2);
	});

	Handlebars.registerHelper('trimString', function(passedString) {
	    var s = passedString.split('turner-swimages/')[1];
	    return new Handlebars.SafeString(s)
	});
});

function getImages(filterType, username){
	console.log('getImages');
	console.log(filterType);
	console.log(username);
    var filters = {};
    if(typeof filterType != 'object'){
        filters.filter = filterType;
        if('username' === filterType){
            filters.username = username;
        }
    }

    $.ajax({
		type: "GET",
		url: "/media",
		data: filters,
		dataType: "text",
		success: function(data, status, exthing){
	  		//console.log('data: ' + data);
	  		//compile the handlebars template
	  		template = Handlebars.compile(imageTemplate);
	  		
	  		//parse the json
	  		var images = JSON.parse(data);
	  		createImages(images);

      		//removes the last comma in the string
      		$('.tagslist').each(function() {
      			var str = $(this).html();
      			str = str.substring(0, str.length - 1);
      			$(this).html(str);
      		});
      		$("img.lazy").lazyload();
		},
		error: function(jqXHR, textStatus, errorThrown){
	  		console.debug(textStatus);
		}
	});
}

function createImages(images){
	var addedSeeMore = false;
	$('.gallery').html('');
	var html = '';

	for (var i = 0; i < images.length; i++) {
		var thisDate = images[i].lastModified.split('T')[0];
		if (dates.indexOf(thisDate) == -1) {
			dates.push(thisDate);
			if (dates.length < 11)
				$('#dates').append('<li data-filter="' + thisDate + '" class="filter-link"><label for="cb' + thisDate + '">' + thisDate + '</label><input type="checkbox" id="cb' + thisDate + '" /></li>');
			if (dates.length == 11 && !addedSeeMore) {
				$('#dates').append('<li id="seemore">...More Dates</li>');
				$('#dates').append("<li id='cleardatefilter'><img src='./Content/images/x.png' width='20' height='20' alt='' /><span>VIEW ALL DATES</span></li>");
				addedSeeMore = true;
				totalDates = 10;
			}
		}

		/******* looping through images in each row to make height even *******/
		$(".gallery").append(template(images[i]));
		if (i % 4 == 3) { //at end of row
			var maxHeight = $('.gallery .item-wrapper')[i].scrollHeight;
			for (var j = i - 1; j > i - 4; j--) {
				//if (i < 4) console.log('orig height ' + $('.gallery .item-wrapper')[j].scrollHeight);
				if (maxHeight < $('.gallery .item-wrapper')[j].scrollHeight)
					maxHeight = $('.gallery .item-wrapper')[j].scrollHeight;
			}
			//if (i < 4) console.log('maxHeight ' + maxHeight);
			for (j = i; j > i - 4; j--) {
				//if (i < 4) console.log($('.gallery .item-wrapper')[j]);
				$('.gallery .item-wrapper')[j].style.height = maxHeight + 'px';
			}
		}
		/******* end new code *********/
	}

	//$(".gallery").html(template(images));

	$('.itemoverlay').hover(selectItem, unselectItem);
	$('.addtoqueue').click(publish);
	$('.kill').click(kill);
	$('.uploader').click(filterByName);

	$('#seemore').click(showMoreDates);
	$('input[type="checkbox"]').change(dateFilter);
	$('#cleardatefilter').click(showAll);
}

function filterImages() {
	var i = 0;
	$('.item-wrapper').each(function() {
		var show = true;
		if (f_dates.length > 0 && f_dates.indexOf($(this).children('.item').data('date').split('T')[0]) == -1)
			show = false;
		
		if (f_usernames.length > 0 && f_usernames.indexOf($(this).children('.imgdata').children('.metadata').children('.uploader').children('span').text()) == -1)
			show = false;
		
		if (f_tags.length > 0) {
			var tags = $(this).children('.imgdata').children('.metadata').children('.tagslist').children('span');
			var match = false;
			for (var i = 0; i < tags.length; i++) {
				if (f_tags.indexOf(tags[i].innerHTML) != -1)
					match = true;
			}
			if (!match) show = false;
		}
	    
	    if (show) {
	    	$(this).show();
	    	i++;
	    	if (i % 4 == 3) { //at end of row
				var maxHeight = $('.gallery .item-wrapper')[i].scrollHeight;
				for (var j = i - 1; j > i - 4; j--) {
					//if (i < 4) console.log('orig height ' + $('.gallery .item-wrapper')[j].scrollHeight);
					if (maxHeight < $('.gallery .item-wrapper')[j].scrollHeight)
						maxHeight = $('.gallery .item-wrapper')[j].scrollHeight;
				}
				//if (i < 4) console.log('maxHeight ' + maxHeight);
				for (j = i; j > i - 4; j--) {
					//if (i < 4) console.log($('.gallery .item-wrapper')[j]);
					$('.gallery .item-wrapper')[j].style.height = maxHeight + 'px';
				}
			}
	    }
	    else
	    	$(this).hide();
	});
	$("img.lazy").lazyload();
}

//add more dates to the filter
function showMoreDates() {
	if (totalDates < dates.length) {
		$('#dates li').last().remove(); //removes view all dates
		$('#dates li').last().remove(); //removes see more dates
		for (var i = totalDates; i < totalDates + 10; i++) {
			if (i < dates.length)
				$('#dates').append('<li data-filter="' + dates[i] + '" class="filter-link"><label for="cb' + dates[i] + '">' + dates[i] + '</label><input type="checkbox" id="cb' + dates[i] + '" /></li>');
			else {
				i--;
				break;
			}
		}
		$('.filters').css('height', 550 + (i - 10) * 31);
		if (i % 10 == 0)	
			$('#dates').append('<li id="seemore">...More Dates</li>');
		$('#dates').append("<li id='cleardatefilter'><img src='./Content/images/x.png' width='20' height='20' alt='' /><span>VIEW ALL DATES</span></li>");
		totalDates += 10;

		$('#seemore').click(showMoreDates);
		$('input[type="checkbox"]').change(dateFilter);
		$('#cleardatefilter').click(showAll);
	}
}

function showAll() {
	shownDates.length = 0;
	$('.item-wrapper').show();
	$('.filter-link input[type="checkbox"]').attr('checked', false);
	$('.heading').text('Showing all content.');
	$("body").toggleClass("filter");
}

//item select handler
function selectItem(e) {
	var sib = $(this).siblings('.item');
	// Array handling
	var id = sib.attr('id');
	var url = $(sib).data('url');

	var item = {"id": id, "url":url};
	selectedImages.push(item);

	$(this).toggleClass("selected");
}

function unselectItem(e) {
	var sib = $(this).siblings('.item');
	var id = sib.attr('id');;
	var url = $(sib).data('url');

	var newArray = [];
	_.each(selectedImages, function(item){
		if(item.id !== id){
			newArray.push({"id":item.id, "url":url});
		}
	});
	selectedImages = newArray;

	$(this).toggleClass("selected");
}

function createUserList(images){
  var userNameDiv = $('#userNames');
  var userNames = _.uniq(_.map(images, function(image){return image.uploader}));
  var userNameHTML = '<ul>';
  _.each(userNames, function(name){
    userNameHTML += ('<li><a href="#"' + name + '" class="filter-link" onclick="filterByName(this);">' + name + '</a><li/>');
  });
  userNameHTML += '</ul>';
  userNameDiv.html(userNameHTML);
}

function publish() {
	console.log('objs: ' + JSON.stringify(selectedImages));
	$.ajax({
		type: "POST",
        url: "/queue",
		data: {"images" : selectedImages},
		success: function(data, status, ex){
            console.log('added to queue!');
			selectedImages = [];
            getImages();
		},
        error: function() {
          console.log('error: cant get to /queue path');
        }
	});
}

function kill() {
	console.log(selectedImages);

	$.ajax({
		type: "POST",
		url: "/kill",
		data: {"images" : selectedImages},
		success: function(data, status, ex) {
			console.log('killed');

			selectedImages = [];
			
			getImages();
		}
	});
}

function dateFilter(e) {
	var dateSelected = e.target.id.split('cb')[1];
	//$('.item-wrapper').each(function() {
		if (e.target.checked) {
			if (f_dates.indexOf(dateSelected) == -1)
				f_dates.push(dateSelected);
		}
		else {
			if (f_dates.indexOf(dateSelected) != -1)
				f_dates.splice(f_dates.indexOf(dateSelected), 1);
		}
	filterImages();
	$('.heading').text(getHeading());
}

function filterByName(e){
	var userName = $(e.target).text();
	f_usernames.push(userName);
	console.log($(e));
	console.log('userName ' + userName);
	//getImages('username', userName);
	filterImages();
	$('.heading').text(getHeading());
}

function filterByTag(e) {
	console.log('filterByTag');
	console.log($(e));
	var tag = $(e).text();
	console.log('tag ' + tag);
	f_tags.push(tag);
	filterImages();
	$('.heading').text(getHeading());
}

function filter() {
	var filterType = $(this).data('filter');

    getImages(filterType);

    var heading = getHeading(filterType);
    $('.heading').text(heading);
}

function getHeading(){
    var heading = 'Showing all content from ';
    $('#clearfilter').show();
    //cycle through dates
    var len = f_dates.length;
    if (len > 0) {
    	heading += 'dates: ';
	    for (var i = 0; i < len; i++)
	    	heading += f_dates[i] + ' & ';
	   	heading = heading.substring(0, heading.length - 2);
    }
	//cycle through usernames
    var len = f_usernames.length;
    if (len > 0) {
    	heading += 'users: ';
	    for (var i = 0; i < len; i++)
	    	heading += f_usernames[i] + ' & ';
	   	heading = heading.substring(0, heading.length - 2);
    }
	//cycle through tags
    var len = f_tags.length;
    if (len > 0) {
    	heading += 'tags: ';
	    for (var i = 0; i < len; i++)
	    	heading += f_tags[i] + ' & ';
	   	heading = heading.substring(0, heading.length - 2);
    }
    if (heading == 'Showing all content from ') {
    	heading = 'Showing all content.';
        $('#clearfilter').hide();
    }
    return heading;
}

function testURL(e) {
	if (e.innerHTML.indexOf('http://') != -1) 
		window.open(e.innerHTML, '_blank');
	else
		alert('This is not a valid url');
}