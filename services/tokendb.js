/**
 * Manages storage, retrieval, and redemption of tokens.
 */

var config = require('./../config');

config.setDev();

var mongodb = require('./../mongo');

/**
 * Saves a new token record
 * @param token
 */
exports.save = function(token) {
    var tokenDocument = {"token" : token };
    mongodb.insert(tokenDocument, 'tokens', function(json){
        console.log('stored token [' + token + ']: ' + json);
    });
};

/**
 * Finds token record(s) matching the given token
 * @param token
 * @param callback
 */
exports.find = function(token, callback) {
    mongodb.find('tokens', {"token" : token }, callback);
};

/**
 * Deletes or 'redeems' a token
 * @param tokens
 */
exports.deleteToken = function(tokens){
    if(tokens.length > 0){
        var first = tokens[0];
        mongodb.removeOne('tokens', first._id);
    }
};

