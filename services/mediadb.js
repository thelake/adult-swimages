/**
 *  Handles insertion, retrieval, publishing, and 'killing' of user-generated media content
 *  metadata into the MongoDB database.
 */

var config = require('./../config');
config.setDev();

var mongodb_driver = require('mongodb');
var mongodb = require('./../mongo');
var _ = require('underscore');

/**
 * Saves image metadata
 */
exports.save = function(metadata) {
  mongodb.insert(metadata, 'media', function(json){
      console.log('stored image [' + metadata.url + ']: ' + json);
  });
};

/**
 * Finds media uploaded between the start date and end date
 * @param startDate
 * @param endDate
 * @param callback - a function to execute with the results
 */
exports.findBetweenDates = function(username, startDate, endDate, callback){
  var criteria = {
    $and:[
      {"published":false},
      {"reviewed":false},
      {"created" : {$gte: startDate, $lt: endDate}}
    ]
  };

  if(username){
    criteria['$and'].push({"uploader" : username});
  }

  mongodb.find('media', criteria, callback);
};

/**
 * Finds all user-generated media that is awaiting review
 * @param callback
 */
exports.findAll = function(username, callback){
  var criteria = {$and:[{"published":false}, {"reviewed":false}]};
  if(username){
    criteria['$and'].push({"uploader" : username});
  }

  mongodb.find('media', criteria , callback);
};

/**
 * Searches all user-generated media that is awaiting review
 * @param callback
 */
exports.searchMedia = function(searchFields, callback){
  var criteria = {$and:[{"published":false}, {"reviewed":false}]};

  if(searchFields.username){
    var username = searchFields.username;
    criteria['$and'].push({"uploader" : username});
  }
  if(searchFields.tags){
    var tags = searchFields.tags;
    criteria['$and'].push({"tags" : tags});
  }

  mongodb.find('media', criteria , callback);
};

/**
 * Creates a new queue
 * @param imageSet
 * @param urls
 * @param successCallback
 */
exports.newQueue = function(imageSet, urls, successCallback){
    mongodb.updateMulti({url:{$in: urls}}, {$set: {reviewed: true}}, 'media', function(multiResult){
        mongodb.insert(imageSet, 'publish', successCallback);
    });
};

/**
 * Adds images to an existing queue
 * @param id queueID
 * @param media
 * @param urls
 * @param successCallback
 */
exports.enqueue = function(id, media, urls, successCallback){
  mongodb.updateMulti({url:{$in: urls}}, {$set: {reviewed: true}}, 'media', function(multiResult){
    mongodb.findOne(id, 'publish', function(queue){
      _.each(media, function(image){
        queue.set.push(image);
      });
      queue.lastModified = new Date();
      mongodb.update(id, queue, 'publish', successCallback);
    });
  });
};

exports.lookupMediaFromMetadata = function(media_metadata, successCallback){
  var ids = getIds(media_metadata);
  mongodb.find('media', {_id: {$in: ids}}, successCallback);
};

/**
 * Removes images from an existing queue
 * @param id queueID
 * @param media
 * @param urls
 * @param successCallback
 */
exports.dequeue = function(id, media, urls, successCallback){
  mongodb.updateMulti({url:{$in: urls}}, {$set: {reviewed: false}}, 'media', function(multiResult){
    mongodb.findOne(id, 'publish', function(queue){
      var set = queue.set;
      _.each(media, function(imageToRemove){
        var removeId = imageToRemove._id.toString();
        _.each(set, function(existingImage, index){
          if(existingImage._id.toString() == removeId){
            set.splice(index, 1);
          }
        })
      });
      queue.lastModified = new Date();
      mongodb.update(id, queue, 'publish', successCallback);
    });
  });
};

/**
 * Marks a set of images as 'reviewed'
 * @param urls
 * @param successCallback
 */
exports.kill = function(urls, successCallback){
    mongodb.updateMulti({url: {$in: urls}}, {$set: {reviewed: true}}, 'media', successCallback);
};

/**
 * Updates meta data of media docs
 * @param mongo json query and json update
 * @param successCallback
 */
exports.update = function(query, update, successCallback){
    mongodb.updateMulti(query, update, "media", successCallback);
};

/**
 * Restores a set of killed images
 * Marks a set of images as not 'reviewed'
 * @param urls
 * @param successCallback
 */
exports.restore = function(urls, successCallback){
    mongodb.updateMulti({url: {$in: urls}}, {$set: {reviewed: false}}, 'media', successCallback);
};

/**
 * Returns a set of reviewed images
 * The definition of killed is (reviewed == true)
 * @param successCallback
 */
exports.findKilled = function( successCallback) {
  mongodb.find('media', {"reviewed":true}, successCallback);
};

function getIds(media_metadata){
  return _.map(media_metadata, function(media){
    return new mongodb_driver.ObjectID(media.id.toString());
  });
}
