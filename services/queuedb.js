/**
 *  Handles insertion, retrieval, publishing, and 'killing' of user-generated media content
 *  metadata into the MongoDB database.
 */

var _ = require('underscore');
var config = require('./../config');
config.setDev();

var mongodb = require('./../mongo');


/**
 * Finds all published media sets
 * @param callback
 */
exports.findAll = function(callback){
  mongodb.findAll('publish', function(docs){
    var docMap = _.map(docs, function(doc){
      return {
        id: doc._id,
        lastModified: doc.lastModified,
        numImages: doc.set.length
      };
    });
    callback(docMap);
  });
};

/**
 * Finds one set of published media
 * @param id
 * @param callback
 */
exports.findById = function(id, callback){
  mongodb.findOne(id, 'publish', callback);
};


/**
 * Finds latest queue
 * @param callback
 */
exports.findLatest = function(callback){
  mongodb.search('publish', {"published" : false}, {}, null, 1, null, callback);
};


/**
 * Finds published queues
 * @param callback
 */
exports.findPublished = function(callback){
  mongodb.search('publish', {"published" : true}, {}, null, null, null, function(docs){
    var docMap = _.map(docs, function(doc){
      return {
        id: doc._id,
        lastModified: doc.lastModified,
        numImages: doc.set.length
      };
    });
    callback(docMap);
  });
};


/**
 * Update an existing queue (e.g. reorder images, mark as published)
 * @param id
 * @param queue
 * @param callback
 */
exports.update = function(id, queue, callback){
  mongodb.update(id, queue, 'publish', callback);
};

