var aws = require('aws-sdk');

var configPath = __dirname + '/../aws_config.json';
console.log('Loading aws config from [' + configPath + ']');

aws.config.loadFromPath(configPath);

var s3_bucket = process.env.S3_BUCKET || 'turner-swimages';
var s3_cdn = process.env.S3_CDN || 'work-release';
var s3 = new aws.S3( { params: { Bucket: s3_bucket } } );

//Initialize the knox environment for
var aws_config = require(__dirname + '/../aws_config.json');
var accessKey = aws_config.accessKeyId;
var secretKey = aws_config.secretAccessKey;

var knox = require('knox').createClient({
    key: accessKey,
    secret: secretKey,
    bucket: s3_bucket
});


module.exports.put = function(filename, contentType, fileData, callback){
  s3.putObject(
      {
        Key: filename,
        ContentType: contentType,
        Body: fileData
      },
      callback
  );
};

/**
 * Downloads a file from s3 and saves in the tmp dir of the project (for transcoding)
 * @param filename
 * @param callback
 */
module.exports.get = function( filename, callback ) {
	var server_file = require('fs').createWriteStream('./tmp/'+filename);
	var params = { 
					Bucket: s3_bucket, 
					Key: filename };
	s3.getObject( params, function(error, data) {
            if ( (error) && (undefined != error.name) ) {
                if (error.message=="The specified key does not exist.") {
                    callback(error);
                }
				console.log("Error - s3.getObject - file " + filename + " | error.message:  " + error.message);
			}
		}).on('error', function(error) {
                if (error.message=="The specified key does not exist.") {
                    callback(error);
                }
                console.log("Error - s3_service.get - " + error.message);
            })
          .on('httpData', function(chunk) { server_file.write(chunk); })
          .on('httpDone', function(error, data) {
                server_file.end();
                console.log("Info - s3_service.get - Retrieived s3 file for transcoding");
                callback(error, data);
           })
          .send();
};

/**
 * Publishes a file to the CDN by moving from s3 bucket turner-swimages to bucket work-release
 * @param src - name of file to be moved
 * @param dest - destination file name
 */
module.exports.move = function(src, dest) {
    knox.copyTo(src, s3_cdn, dest).on('response', function (res, error) {
        console.log("Info - s3_service.move - copying " + src + " to " + s3_cdn);
        console.log(res.statusCode);
        console.log(res.headers);
        console.log("<end-copy>");

        knox.del(src).on('response', function (res, error) {
            console.log("<begin-delete>");
            console.log(res.statusCode);
            console.log(res.headers);
            console.log("<end-delete>");
        }).once('error', logError('knox.del'))
            .end();
    }).once('error', logError('knox.copyTo'))
        .end();
}

logError = function (message) {
    console.log('Warning - ' + message + ' possible socket error in s3_service.move');
}