var express = require('express'),
  cors = require('cors'),
	path = require('path'),
	config = require('./config'),
	aws = require('./controllers/aws'),
  site = require('./controllers/site'),
  video = require('./controllers/video');

config.setDev();
var mongo = require('./mongo');


var app = exports.app = express();
var upload_path = './tmp';

var whitelist = [
  'http://www.adultswim.com',
  'https://www.adultswim.com',
  'http://adultswim.com',
  'https://www.adultswim.com',
  'http://staging3.adultswim.com',
  'http://swimages.tbs.io',
  'https://staging3.adultswim.com'
];

var corsOptionsDelegate = function(req, callback){
  var corsOptions;
  if(whitelist.indexOf(req.header('Origin')) !== -1){
    corsOptions = { origin: true }; // reflect (enable) the requested origin in the CORS response
  }else{
    corsOptions = { origin: false }; // disable CORS for this request
  }
  callback(null, corsOptions); // callback expects two parameters: error and options
};

app.use(express.limit(1 * 1024 * 1024 * 1024));

app.configure(function(){
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.methodOverride());
  app.use(express.cookieParser());
  app.use(express.bodyParser({
    keepExtensions: false,
    uploadDir: upload_path,
    hash: 'md5'
  }));
  app.use(cors(corsOptionsDelegate));
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

/* upload api */
//app.post('/publish', site.token);
app.post('/upload', aws.handle_upload);
app.get('/token', aws.generate_token);

/* video api */
app.post('/transcode', video.handle_transcoding);

/* site api */
app.get('/media', site.getMedia);
app.get('/media/search', site.searchMedia);
app.post('/queue', site.newQueue);

app.post('/kill', site.kill);
app.get('/killed', site.getKilledMedia);
app.get('/queue', site.getQueuedMedia);
app.get('/queue/published', site.getPublishedMedia);
app.get('/queue/latest', site.getLatestQueuedMedia);

app.post('/queue/:id/enqueue', site.enqueue);
app.post('/queue/:id/dequeue', site.dequeue);
app.get('/queue/:id', site.getQueuedMediaById);
app.put('/queue/:id', site.publish);

app.post('/restore', site.restore);

app.get('/', function(req, res){
	res.sendfile(__dirname + '/public/index.html');
});

app.get('/test', function(req, res){
    res.sendfile(__dirname + '/public/test_upload.html');
});

app.listen(process.env.PORT || 3000);
