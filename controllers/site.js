var config = require('./../config'),
    _ = require('underscore'),
    moment = require('moment'),
    mediaDB = require('./../services/mediadb'),
    queueDB = require('./../services/queuedb');

exports.getMedia = function(req, res) {
  var filter = req.param('dateFilter');
  var username = req.param('username');

  switch(filter){
      case 'today':
          getMediaByDate(username, res, 1);
          break;
      case 'thisWeek':
          getMediaByDate(username, res, 7);
          break;
      case 'all':
          getAllMedia(username, res);
          break;
      default:
          getAllMedia(username, res);
  }
};

exports.searchMedia = function(req, res){
  var tag = req.param('tag');

  var searchFields = {};

  if(tag){
    searchFields.tags = tag;
  }

  mediaDB.searchMedia(searchFields, function(docs){
    res.send(docs);
  });
};

exports.getQueuedMedia = function(req, res){
  queueDB.findAll(function(docs){
     res.send(docs);
  });
};

exports.getPublishedMedia = function(req, res){
  queueDB.findPublished(function(docs){
    res.send(docs);
  });
};

exports.getLatestQueuedMedia = function(req, res){
  queueDB.findLatest(function(docs){
    if(docs.length === 0){
      res.send(404, 'No queues available');
    }
    else{
      res.send(docs[0]);
    }
  });
};

exports.getQueuedMediaById = function(req, res){
  var id = req.param('id');
  queueDB.findById(id, function(docs){
    res.send(docs);
  });
};


/**
 * Group a set of media items into a queue
 * @param req a request containing a JSON array of media items
 * @param res
 */
exports.newQueue = function(req, res) {
    console.log('inside newQueue');
	var media = req.body.images || [];
	var mediaSet = {"set" : media, "published": false};
	var urls = getUrls(media);
    var successCallback = function(result){
        res.send(result);
    };
    console.log('ms: ' + JSON.stringify(mediaSet));
    mediaDB.newQueue(mediaSet, urls, successCallback);
};

/**
 * Add a set of images to a queue
 * @param req a request containing a JSON array of media items
 * @param res
 */
exports.enqueue = function(req, res) {
  var id = req.param('id');
  var media_metadata = req.body.images;
  var urls = getUrls(media_metadata);

  var successCallback = function(result){
    console.log('enqueued images');
    res.send(result);
  };

  mediaDB.lookupMediaFromMetadata(media_metadata, function(media){
    mediaDB.enqueue(id, media, urls, successCallback);
  });


};

/**
 * Remove a set of images from a queue
 * @param req a request containing a JSON array of media items
 * @param res
 */
exports.dequeue = function(req, res) {
  var id = req.param('id');
  var media_metadata = req.body.images;
  console.log(media_metadata);
  var urls = getUrls(media_metadata);

  var successCallback = function(result){
    console.log('dequeued images');
    res.send(result);
  };

  mediaDB.lookupMediaFromMetadata(media_metadata, function(media){
    mediaDB.dequeue(id, media, urls, successCallback);
  });

};

exports.publish = function(req, res){
  var id = req.param('id');
  var media_metadata = req.body.images;
  var published = (req.body.published === 'true');

  mediaDB.lookupMediaFromMetadata(media_metadata, function(media){

    //force order to be the same as metadata
    var sortedMedia = [];
    _.each(media_metadata, function(img){
      var img_id = img.id;
      _.each(media, function(img_doc){
        var img_doc_id = img_doc._id.toString();
        if(img_doc_id == img_id){
          sortedMedia.push(img_doc);
        }
      });
    });

    var mediaSet = {"set" : sortedMedia, "published" : published, "lastModified": new Date()};
    queueDB.update(id, mediaSet, function(result){
      console.log('published queue');
      res.send(result);
    });
  });
};

exports.token = function(req, res) {
    console.log('at publish');
    res.send('at publish');
};

/**
 * Mark the given media items as "reviewed" but not published
 * @param req a request containing a JSON array of media items
 * @param res
 */
exports.kill = function(req, res) {
	var images = req.body.images;
	var urls = getUrls(images);
    var successCallback = function(result){
        res.send('ok');
    };

    mediaDB.kill(urls, successCallback);
};
 
exports.getKilledMedia = function(req, res) {
  var successCallback = function(result){
    res.send(result);
  };
    mediaDB.findKilled( successCallback );
};

exports.getKilledMedia = function(req, res) {
	var successCallback = function(result){
		res.send(result);
	};
    mediaDB.findKilled( successCallback );
};

/**
 * Restore the given media from a killed state
 * (i.e., Mark the given media items as NOT "reviewed")
 * @param req a request containing a JSON array of media items
 * @param res
 */
exports.restore = function(req, res) {
    var images = req.body.images;
    var urls = getUrls(images);
    var successCallback = function(result){
        res.send('ok');
    };

    mediaDB.restore(urls, successCallback);
};

function getUrls(media) {
	var urls = [];
	_.each(media, function(item){ urls.push(item.url); });
	return urls;
}

function getAllMedia(username, res) {
    var successCallback = function(docs){
        res.send(docs);
    };

    mediaDB.findAll(username, successCallback);
}

function getMediaByDate(username, res, daysAgo) {
    var now = moment();
    var then = now.clone().subtract('days', daysAgo);
    var successCallback = function(docs){
        res.send(docs);
    };

    mediaDB.findBetweenDates(username, then.toDate(), now.toDate(), successCallback);
}