var uuid = require('uuid'),
    mime = require('mime'),
    fs = require('fs'),
    hbjs = require("handbrake-js"),
    path = require('path'),
    mediaDB = require('./../services/mediadb'),
    mime = require("mime"),
    queueDB = require('./../services/queuedb'),
    s3_service = require('./../services/s3_service');

    var s3_url = "https://s3.amazonaws.com/turner-swimages/";
    var mp4_filename = "";
    var filename = "";

/**
 * Download video file from s3 to ~/tmp
 * @param req a request containing the name of the s3 file to be transcoded
 * @param res
 */
    exports.handle_transcoding = function(req, res) {
    	filename = req.param('filename');
        mp4_filename = filename.replace(/\.[^/.]+$/, "") + ".mp4";
    	console.log("Info - video.handle_transcoding " + filename);

        //download file from s3
	    s3_service.get( filename, function(error){
		    if ( (error) && (undefined != error.name) ) {
                console.log("Error - video.download_video - error thrown getting " + filename + " from cloud");
                log_error(error);
                if ((undefined != error.name) && (error.name !== "NetworkingError")) {
                    return res.json(400, {error: error.message + " - " + filename});
                    return;
                }
            }
            transcodeFile(req, res);
	    });
        //res.json(200, {'mp4_url': s3_url + mp4_filename});
    };


/**
 * Transcode file in ./tmp
 * @param req
 * @param res
 */
transcodeFile = function(req, res) {
    if (!isVideoFile('./tmp/' + filename)) {
        console.log("Error - video.transcodeFile - unsupported video format (" + mime.lookup('./tmp/'+filename) + ") for file " + filename);
        return res.json(400, {error: "unsupported video format (" + mime.lookup('./tmp/'+filename) + ") for file " + filename});
    }
    hbjs.exec({ input: './tmp/' + filename, output: './tmp/' + mp4_filename }, function (error, stdout, stderr) {
        console.log("Info - video.transcodeFile " + "./tmp/" + filename);
        if (undefined != error) {
            console.log("Error - video.transcode for " + filename);
            log_error(error);
            console.log(stdout);
            return res.json(400, {error: error.message});
        }
        console.log("Info - video.transcodeFile " + "./tmp/" + filename);
        uploadTranscodedFile(req, res);
    });
};


/**
 * Upload the transcoded file to s3 bucket turner-swimages AND update mongo metadata for this media item
 *
 */
uploadTranscodedFile = function(req, res) {
    var transcoded_file = fs.readFileSync('./tmp/' + mp4_filename);
//    if (!file_exists('./tmp/' + mp4_filename)) {
//        console.log("Error - video.uploadTranscodedFile - transcoded file " + mp4_filename + " does not exist");
//        return;
//    } else {
//        transcoded_file = fs.readFileSync('./tmp/' + mp4_filename);
//    }

    s3_service.put(mp4_filename, "video/mp4", transcoded_file, function (error) {
        if ( (error) && (undefined != error.name) ) {
            console.log("Error - video - error thrown saving transcoded file to aws s3");
            log_error(error);
            return res.json(400, {error: error.message});
        }

        //update media url in mongo
        var query = {url: s3_url + filename };
        var update = { $set: {url: s3_url + mp4_filename }}
        mediaDB.update(query, update, function (error) {
            if ( (error) && (undefined != error.name) ) {
                console.log("Error - video - error thrown updating mongo metadata for " + mp4_filename);
                log_error(error);
                return res.json(400, {error: error.message});
            }
            console.log("200, { mp4_url: " + s3_url + mp4_filename + " }");
            res.json(200, {'mp4_url': s3_url + mp4_filename});
        });
    });
};

log_error = function(error) {
    console.log( "Error.code:  " + error.code + " | " + "Error.err:  " + error.err  + " | " + "Error.name:  " + error.name  + " | " + "Error.message:  " + error.message );
};

/**
 * Determines if the file is a supported mime type for transcoding to mpeg4
 *
 */
isVideoFile = function( filename ) {
    var mime_type = mime.lookup(filename);

    switch (mime_type) {
        case "video/3gpp":                  //.3gp
        case "video/mp4":                   //.mp4
        case "video/x-m4v":                 //.m4v
        case "video/mpeg":                  //.mpg
        case "application/octet-stream":    //.ogm
        case "video/ogg":                   //.ogv
        case "video/quicktime":             //.mov
        case "video/webm":                  //.webm
        case "video/x-matroska":            //.mkv
        case "video/x-ms-wmv":              //.wmv
        case "video/x-msvideo":             //.avi
            console.log("Info - " + filename + " is valid video file");
            return true;
        default:
            return false;
    }
};

file_exists = function( filename ) {
    fs.exists('./tmp/' + filename, function (exists) {
        if (exists) {
            return (true);
        } else {
            return(false);
        }
    });
};