var uuid = require('uuid'),
	mime = require('mime'),
	fs = require('fs'),
	path = require('path'),
  mediaDB = require('./../services/mediadb'),
  tokenDB = require('./../services/tokendb'),
  s3_service = require('./../services/s3_service');

var upload_path = './tmp';

exports.handle_upload = function(req, res, next) {
  //validate token is present
  var token = req.param('token');
  if(!token){
      return res.send(400, 'Token is not present, please try again.');
  }

  //validate request contains a file
  var file = req.files.file;
  if (file === undefined) {
    return res.send(400, 'You must include a file.');
  }
  console.dir("file.name:  " + file.name);
  var type = mime.lookup(file.name);
  var filename = uuid.v4() + '.' + mime.extension(type);
  var fileSize = (file.size || 0) / 1024; //size in kb

  /* this is pretty hacky and ineffecient. No real reason to read this file.
     aws-sdk has no stream interface?? */
  fs.readFile(file.path, function read(err, data) {
    if (err) {
      res.send(500, 'Error reading uploaded media.');
    }
    else {
      //verify token is valid
      tokenDB.find(
        token,
        function(validTokens){
          if(validTokens.length === 0){
            res.send(403, "Invalid token. Please try again after obtaining a new token.");
          }
          else{ //valid token
            s3_service.put(filename, type, data, function(err, data) {
              if (err) {
                res.send(500, 'Upload error');
              }
              else {
                var username = req.param('username', 'defaultuser');
                var metadata = buildMetadata(req, fileSize, filename);
                mediaDB.save(metadata);
                tokenDB.deleteToken(validTokens); //'redeem' token
                res.send(200, "Thanks");
                unlinkFile(file);
              }
            });
          }
        }
      );
    }
  });
};

/*
   A token is used as a way to authorize an image upload. Tokens live in the DB for a short period.
 */
exports.generate_token = function(req, res, next) {
  var token = uuid.v4();
  tokenDB.save(token);
  res.json({ token: token });
};

/*
  cleanup old uploads. more than likely after an unhandled error.
  I figure 5 minutes is long enough.  We can adjust.
*/
function dir_cleanup() {
    var threshold = new Date().getTime() - 5 * 60 * 1000;
    fs.readdir(
        upload_path,
        function( err, files ) {
            if ( err ) return console.log( err );
            files.forEach(function( file ) {
                if(file == '.keep'){
                  return; //skip this file to keep /tmp in source control
                }
                var filePath = upload_path + '/' + file;
                fs.stat(
                    filePath,
                    function( err, stat ) {
                        if ( err ) {
                            return console.log( err );
                        }
                        if ( stat.ctime.getTime() < threshold ) {
                            console.log("will unlink " + filePath);
                            fs.unlink(
                                filePath,
                                function( err ) {
                                    if ( err ) {
                                        return console.log( err );
                                    }
                                }
                            );
                        }
                    }
                );
            });
        }
    );
}

setInterval(dir_cleanup, 1 * 60 * 1000);

function unlinkFile(file) {
    fs.unlink(file.path,
        function (err) {
            if (err) console.log(err);
        }
    );
}

function buildMetadata(req, fileSize, filename){
  var credit_url = req.body.credit_url;
  var username = req.body.username;
  var email = req.body.email || 'don.browning@turner.com';
  var title = req.body.title || 'Test Title';
  var tags = buildTags(req.body.tags);

  var metadata = {
    "title" : title,
    "url" : "https://s3.amazonaws.com/turner-swimages/" + filename,
    "size" : fileSize,
    "uploader" : username,
    "email" : email,
    "starred" : false,
    "reviewed" : false,
    "published" : false,
    "credit_url" : credit_url,
    "tags" : tags
  };

  return metadata;
}

function buildTags(tags){
  var tagArray = [];
  if(tags){
    tagArray = tags.split(',');
  }
  return tagArray;
}


