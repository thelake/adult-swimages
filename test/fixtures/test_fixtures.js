module.exports = {
  latest: {
    "set": [
      {
        "imageId": "529250affc35ff0000000001",
        "url": "https://s3.amazonaws.com/turner-swimages/coaster04.jpg"
      },
      {
        "imageId": "529250affc35ff0000000001",
        "url": "https://s3.amazonaws.com/turner-swimages/coaster04.jpg"
      }
    ],
    "published": false,
    "lastModified": "2014-01-09T18:19:48.568Z",
    "created": "2014-01-09T18:19:48.568Z",
    "_id": "52cee844347b75002657332e"
  },
  byId: {
  "set": [
    {
      "imageId": "529250affc35ff0000000001",
      "url": "https://s3.amazonaws.com/turner-swimages/coaster04.jpg"
    },
    {
      "imageId": "529250affc35ff0000000001",
      "url": "https://s3.amazonaws.com/turner-swimages/coaster04.jpg"
    }
  ],
    "published": false,
    "lastModified": "2014-01-09T18:19:48.568Z",
    "created": "2014-01-09T18:19:48.568Z",
    "_id": "52cee844347b75002657332e"
  },
  published: [
    {
      "id": "52d442354b94db633c9f632e",
      "lastModified": "2014-01-13T19:46:35.342Z",
      "numImages": 3
    },
    {
      "id": "52d41c2f18b2232e391e2d45",
      "lastModified": "2014-01-13T18:36:06.315Z",
      "numImages": 2
    }
  ],
  findAll: [{id: 1, lastModified: '2014-01-10T16:50:27.637Z', numImages: 2}],
  success: { ok: true }
};