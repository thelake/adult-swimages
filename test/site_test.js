/*jshint expr: true*/

var supertest = require('supertest');
var app = require('../app').app;
var request = supertest(app);
var sinon = require('sinon');
var should_static = require('should');

var media_fixture = require('./fixtures/media.json');
var test_fixtures = require('./fixtures/test_fixtures');

var mediaDB = require('../services/mediadb');
var queueDB = require('../services/queuedb');

describe('the media api', function(){
  before(function(){
    sinon.stub(mediaDB, 'findAll').yields(media_fixture);
    sinon.stub(mediaDB, 'searchMedia').yields(media_fixture);
    sinon.stub(mediaDB, 'findBetweenDates').yields(media_fixture);

    sinon.stub(mediaDB, 'kill').yields(test_fixtures.success);
    sinon.stub(mediaDB, 'enqueue').yields(test_fixtures.success);
    sinon.stub(mediaDB, 'newQueue').yields(test_fixtures.success);

    sinon.stub(queueDB, 'findAll').yields(test_fixtures.findAll);
    sinon.stub(queueDB, 'findLatest').yields(test_fixtures.latest);
    sinon.stub(queueDB, 'findById').yields(test_fixtures.latest);
    sinon.stub(queueDB, 'findPublished').yields(test_fixtures.published);
  });

  after(function(){
    mediaDB.findAll.restore();
    mediaDB.findBetweenDates.restore();
    mediaDB.kill.restore();
    mediaDB.newQueue.restore();
    mediaDB.searchMedia.restore();

    queueDB.findAll.restore();
    queueDB.findLatest.restore();
    queueDB.findPublished.restore();
    queueDB.findById.restore();
  });

  describe("/", function(){
    it('GET should return the homepage', function(done){
      request.get('/').end(function(err, resp){
        resp.should.have.status(200);
        done();
      });
    });
  });
  describe("/test", function(){
    it('GET should return the upload test page', function(done){
      request.get('/test').end(function(err, resp){
        resp.should.have.status(200);
        done();
      });
    });
  });
  describe("/media", function(){
    it('GET should return all media', function(done){
      request.get('/media').end(function(err, resp){
        resp.should.have.status(200);
        var images = resp.body;
        images.should.not.be.empty;
        images.length.should.equal(5);
        done();
      });
    });
    it('GET should return all media and set CORS headers', function(done){
      request
        .get('/media')
        .set('Origin', 'http://www.adultswim.com')
        .end(function(err, resp){
          resp.should.have.status(200);
          resp.get('Access-Control-Allow-Origin').should.equal('http://www.adultswim.com');

          var images = resp.body;
          images.should.not.be.empty;
          images.length.should.equal(5);
          done();
      });
    });
    it('GET should return all media and not set CORS headers for non-whitelisted domain', function(done){
      request
        .get('/media')
        .set('Origin', 'http://www.google.com')
        .end(function(err, resp){
          resp.should.have.status(200);

          var corsHeader = resp.get('Access-Control-Allow-Origin');
          should_static(corsHeader).not.be.ok; //hackery to test undefined

          var images = resp.body;
          images.should.not.be.empty;
          images.length.should.equal(5);
          done();
        });
    });
    it('GET should return all media with filter: all', function(done){
      request.get('/media?dateFilter=all').end(function(err, resp){
        resp.should.have.status(200);
        var images = resp.body;
        images.should.not.be.empty;
        images.length.should.equal(5);
        done();
      });
    });
    it('GET should return all media with filter: all and username', function(done){
      request.get('/media?dateFilter=all&username=Nei').end(function(err, resp){
        resp.should.have.status(200);
        var images = resp.body;
        images.should.not.be.empty;
        images.length.should.equal(5);
        done();
      });
    });
    it('GET should return all media with filter: today', function(done){
      request.get('/media?dateFilter=today').end(function(err, resp){
        resp.should.have.status(200);
        var images = resp.body;
        images.should.not.be.empty;
        images.length.should.equal(5);
        done();
      });
    });
    it('GET should return all media with filter: thisWeek', function(done){
      request.get('/media?dateFilter=thisWeek').end(function(err, resp){
        resp.should.have.status(200);
        var images = resp.body;
        images.should.not.be.empty;
        images.length.should.equal(5);
        done();
      });
    });
  });

  describe("/media/search", function(){
    it('GET should return all media matching tag', function(done){
      request.get('/media/search?tag=funny')
        .end(function(err, resp){
          resp.should.have.status(200);
          var images = resp.body;
          images.should.not.be.empty;
          images.length.should.equal(5);
          done();
        });
      });
  });

  describe("/queue", function(){
    it('POST should return 200 OK', function(done){
      request.post('/queue')
          .send(media_fixture)
          .end(function(err, resp){
            resp.should.have.status(200);
            done();
          });
      });
  });
  describe("/kill", function(){
    it('POST should return 200 OK', function(done){
      request.post('/kill')
          .send(media_fixture)
          .end(function(err, resp){
            resp.should.have.status(200);
            done();
          });
    });
  });
  describe("/queue", function(){
    it('GET should return a listing of all queued media sets', function(done){
      request.get('/queue').end(function(err, resp){
        resp.should.have.status(200);
        done();
      });
    });
    it('GET should return the most recent unpublished media set', function(done){
      request.get('/queue/latest').end(function(err, resp){
        resp.should.have.status(200);
        done();
      });
    });
    it('GET should return all published media sets', function(done){
      request.get('/queue/published').end(function(err, resp){
        resp.should.have.status(200);
        done();
      });
    });
  });
});
