/*jshint expr: true*/

var supertest = require('supertest');
var app = require('../app').app;
var request = supertest(app);
var sinon = require('sinon');

var tokenDB = require('../services/tokendb');
var mediaDB = require('../services/mediadb');
var s3_service = require('../services/s3_service');

describe('the api', function(){
  describe("/token", function(){
    before(function(){
      sinon.stub(tokenDB, 'find').yields('ABC123');
    });
    after(function(){
      tokenDB.find.restore();
    });
    it('GET should generate a token', function(done){
      request.get('/token').end(function(err, resp){
        resp.should.have.status(200);
        resp.body.token.should.not.be.empty;
        done();
      });
    });
  });

  describe('/upload', function(){
    before(function(){
      sinon.stub(tokenDB, 'save');
      sinon.stub(tokenDB, 'deleteToken');
      sinon.stub(tokenDB, 'find').yields('ABC123');
      sinon.stub(mediaDB, 'save');

      sinon.stub(s3_service, 'put').yields(undefined, { success: true });
    });

    after(function(){
      tokenDB.save.restore();
      tokenDB.find.restore();
      tokenDB.deleteToken.restore();

      mediaDB.save.restore();

      s3_service.put.restore();
    });
    it('POST should upload an image and store metadata', function(done){
      request
        .get('/token')
        .end(function(err, res){
          var token = res.body.token;
          token.should.not.be.empty;

          request
              .post('/upload')
              .attach('file', './test/fixtures/nice.jpeg')
              .field('username', 'test')
              .field('email', 'neil.shannon@turner.com')
              .field('tags', 'action,test,foo')
              .field('token', token)
              .end(function(err, resp){
                resp.should.have.status(200);
                done();
              });
          });
    });
    it('POST should return HTTP 400 if the token parameter is missing', function(done){
      request
        .post('/upload')
        .attach('file', './test/fixtures/nice.jpeg')
        .field('username', 'test')
        .end(function(err, resp){
          resp.should.have.status(400);
          done();
        });
    });
    it('POST should return HTTP 403 if the token parameter is invalid/expired', function(done){
      tokenDB.find.yields([]);

      request
        .post('/upload')
        .attach('file', './test/fixtures/nice.jpeg')
        .field('token', 'ABC123')
        .field('username', 'test')
        .end(function(err, resp){
          resp.should.have.status(403);
          done();
        });

      tokenDB.find.yields('ABC123');
    });
    it('POST should return HTTP 500 if upload to s3 fails', function(done){
      s3_service.put.yields({error: 'something went wrong'}, undefined);

      request
        .post('/upload')
        .attach('file', './test/fixtures/nice.jpeg')
        .field('token', 'ABC123')
        .field('username', 'test')
        .end(function(err, resp){
          resp.should.have.status(500);
          done();
        });
    });
    it('POST should return HTTP 400 if the file to upload is missing', function(done){
      request
        .post('/upload')
        .field('username', 'test')
        .field('token', 'ABC123')
        .end(function(err, resp){
          resp.should.have.status(400);
          done();
        });
    });
  });
});
